const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const tests = b.option(bool, "Tests", "Build tests [default: false]") orelse false;

    const libZix = b.addStaticLibrary(.{
        .name = "zix",
        .target = target,
        .optimize = optimize,
    });
    libZix.addIncludePath(.{ .path = "include" });
    libZix.addIncludePath(.{ .path = "src" });
    libZix.defineCMacro("ZIX_STATIC", null);
    libZix.defineCMacro("_GNU_SOURCE", null);
    libZix.addCSourceFiles(&.{
        "src/allocator.c",
        "src/btree.c",
        "src/bump_allocator.c",
        "src/digest.c",
        "src/errno_status.c",
        "src/filesystem.c",
        "src/hash.c",
        "src/path.c",
        "src/ring.c",
        "src/status.c",
        "src/string_view.c",
        "src/system.c",
        "src/tree.c",
    }, cflags);
    libZix.addCSourceFiles(switch (target.getOsTag()) {
        .windows => &.{
            "src/win32/filesystem_win32.c",
            "src/win32/sem_win32.c",
            "src/win32/system_win32.c",
            "src/win32/thread_win32.c",
        },
        .macos => &.{
            "src/darwin/sem_darwin.c",
            "src/posix/system_posix.c",
            "src/posix/thread_posix.c",
        },
        else => &.{
            "src/posix/filesystem_posix.c",
            "src/posix/sem_posix.c",
            "src/posix/system_posix.c",
            "src/posix/thread_posix.c",
        },
    }, cflags);
    libZix.linkLibC();
    libZix.installHeadersDirectory("include", "");

    b.installArtifact(libZix);

    if (tests) {
        // buildBench(b, .{
        //     .lib = libZix,
        //     .path = "benchmark/tree_bench.c",
        // });
        // buildBench(b, .{
        //     .lib = libZix,
        //     .path = "benchmark/dict_bench.c",
        // });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_tree.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_thread.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_ring.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_path.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_digest.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_sem.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_status.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_hash.c",
        });
        if (!target.isDarwin()) buildBench(b, .{
            .lib = libZix,
            .path = "test/test_filesystem.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/test_allocator.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/headers/test_headers.c",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/cpp/test_headers_cpp.cpp",
        });
        buildBench(b, .{
            .lib = libZix,
            .path = "test/cpp/test_path_std.cpp",
        });
    }
}

fn buildBench(b: *std.Build, info: BuildInfo) void {
    const exe = b.addExecutable(.{
        .name = info.filename(),
        .optimize = info.lib.optimize,
        .target = info.lib.target,
    });
    for (info.lib.include_dirs.items) |include| {
        exe.include_dirs.append(include) catch {};
    }
    exe.defineCMacro("ZIX_STATIC", null);
    exe.defineCMacro("ZIX_INTERNAL", null);
    exe.addIncludePath(.{ .path = "benchmark" });
    exe.addIncludePath(.{ .path = "test" });
    if (std.mem.endsWith(u8, info.path, ".cpp"))
        exe.addCSourceFile(.{ .file = .{ .path = info.path }, .flags = &.{
            "-Wall",
            "-Wextra",
            "-Wno-bad-function-cast",
            "-Wno-c11-extensions",
            "-Wno-declaration-after-statement",
            "-Wno-implicit-fallthrough",
            "-Wno-padded",
        } })
    else
        exe.addCSourceFile(.{ .file = .{ .path = info.path }, .flags = cflags });
    if (std.mem.startsWith(u8, info.filename(), "test")) {
        exe.addCSourceFile(.{ .file = .{ .path = "test/failing_allocator.c" }, .flags = cflags });
    }
    if (exe.target.isWindows())
        exe.subsystem = .Console;
    exe.linkLibrary(info.lib);
    if (std.mem.endsWith(u8, info.path, ".cpp") and exe.target.getAbi() != .msvc) {
        exe.linkLibCpp();
    }
    exe.linkLibC();
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step(
        b.fmt("{s}", .{info.filename()}),
        b.fmt("Run the {s} test", .{info.filename()}),
    );
    run_step.dependOn(&run_cmd.step);
}

const cflags = &.{
    "-Wall",
    "-Wextra",
    "-Wno-bad-function-cast",
    "-Wno-c11-extensions",
    "-Wno-declaration-after-statement",
    "-Wno-implicit-fallthrough",
    "-Wno-padded",
    "-std=gnu99",
};

const BuildInfo = struct {
    lib: *std.Build.Step.Compile,
    path: []const u8,

    fn filename(self: BuildInfo) []const u8 {
        var split = std.mem.splitSequence(u8, std.fs.path.basename(self.path), ".");
        return split.first();
    }
};
